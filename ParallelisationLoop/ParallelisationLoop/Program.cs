﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelisationLoop
{
	class Program
	{
		static void Main(string[] args)
		{
			int max = 10000;

			/*
             * Attention sur les listes, l'index n'est pas threadSafe, il y a donc un risque de perte de donnée.
             * Il vaut mieux utiliser des tableaux
             */
			var l1 = new int[max];
			//remplissage
			Console.WriteLine("Filling in single thread.");
			Stopwatch chRemplissage = new Stopwatch();
			chRemplissage.Start();
			for (int i = 0; i < max; i++)
			{
				l1[i] = i;
				Thread.Sleep(1);
			}
			chRemplissage.Stop();
			Console.WriteLine(chRemplissage.Elapsed.ToString());
			Console.WriteLine($"l1 own {l1.Length} elements");

			Console.WriteLine("Multiplication in single thread.");
			Stopwatch chMultipli = new Stopwatch();
			chMultipli.Start();
			foreach (var i1 in l1)
			{
				int i2 = i1 * 2;
				Thread.Sleep(1);
			}
			chMultipli.Stop();
			Console.WriteLine(chMultipli.Elapsed.ToString());

			Console.WriteLine("\n");

			Console.WriteLine("filling in parallel.");
			/*
             * Attention sur les listes, l'index n'est pas threadSafe, il y a donc un risque de perte de donnée.
             * Il vaut mieux utiliser des tableaux
             */
			l1 = new int[max];
			chRemplissage = new Stopwatch();
			chRemplissage.Start();
			Parallel.For(1, max, (indice) =>
			{
				l1[indice] = indice;
				Thread.Sleep(1);
			});
			chRemplissage.Stop();
			Console.WriteLine(chRemplissage.Elapsed.ToString());
			Console.WriteLine($"l1 own {l1.Length} elements");

			Console.WriteLine("Multiplication in parallel.");
			chMultipli = new Stopwatch();
			chMultipli.Start();
			Parallel.ForEach(l1.Skip(1), (valeur, etat, indice) =>
			{
				int val2 = valeur * 2;
				Thread.Sleep(1);
			});
			chMultipli.Stop();
			Console.WriteLine(chMultipli.Elapsed.ToString());

			Console.ReadLine();
		}
	}
}
