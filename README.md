# CsharpAsync

POC of loop parallelization vs classical loop

![Screenshot](/res/cmd_parralelisation.png)

```csharp
Console.WriteLine("filling in parallel.");
/*
 * Attention sur les listes, l'index n'est pas threadSafe, il y a donc un risque de perte de donnée.
 * Il vaut mieux utiliser des tableaux
 */
l1 = new int[max];
chRemplissage = new Stopwatch();
chRemplissage.Start();
Parallel.For(1, max, (indice) =>
{
    l1[indice] = indice;
    Thread.Sleep(1);
});
chRemplissage.Stop();
Console.WriteLine(chRemplissage.Elapsed.ToString());
Console.WriteLine($"l1 own {l1.Length} elements");

Console.WriteLine("Multiplication in parallel.");
chMultipli = new Stopwatch();
chMultipli.Start();
Parallel.ForEach(l1.Skip(1), (valeur, etat, indice) =>
{
    int val2 = valeur * 2;
    Thread.Sleep(1);
});
chMultipli.Stop();
Console.WriteLine(chMultipli.Elapsed.ToString());
```